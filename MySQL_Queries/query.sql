
-- Create database
CREATE DATABASE safe_ride;


-- create table
CREATE TABLE schedules ( full_name text, uoid text, email text, phone text, party text, pickup text, dropoff text, location text, comments text, vehicle_no text);

-- Display everything on table
select * from schedules;

-- order by pickup_time
SELECT full_name, uoid, email, phone, pickup_time, party, pickup, dropoff, location, comments, vehicle_no, color, time_stamp from schedules order by pickup_time;

-- insert column 
alter table schedules add time_stamp TIMESTAMP;


-- Delete all from table
DELETE * FROM schedules;


-- Delete Null values
delete from schedules where full_name IS NULL;

