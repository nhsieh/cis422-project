<?php

include('connectionData.php');

// Create connection
$sql_con = new mysqli($server, $username, $password, $dbname, $port);

// Check connection
if ($sql_con->connect_error) {
    die("Connection failed: ".$sql_con->connect_error);
} 

?>
<!DOCTYPE html>
<html>
	<head>
		<title>SafeRide Schedule</title>
		<!-- <link rel="stylesheet" href="static/css/normalize.css"> -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	</head>

	<body>
		<h2>Safe Ride</h2>

		<hr>
		<div class="container">
			<h2>Dispatcher View of Schedule Table</h2>
			<p></p>
			<div class='table-responsive'>
				<table class="table table-striped table-hover table-bordered">
					<thead>
						<tr>
							<th>Time Requested</th>
							<th>Rider's Name</th>
							<th>UO ID #</th>
							<th>Email</th>
							<th>Phone Number</th>
							<th>Pick Up Time</th>
							<th>Party #</th>
							<th>Pick Up Location</th>
							<th>Dropoff Destination</th>
							<th>Location (remove later)</th>
							<th>Comments</th>
							<th>Vehicle #</th>

						</tr>
					</thead>
					<tbody>

					<?php

						$user_name = $_POST['username'];
						$pwd = $_POST['password'];

						if ($stmt = $sql_con->prepare("SELECT full_name, uoid, email, phone, pickup_time, party, pickup, dropoff, location, comments, vehicle_no, color, time_stamp from schedules order by pickup_time"))
						$stmt->execute();
						$stmt->bind_result($name, $uoid, $email, $phone, $pickup_time, $party, $pickup, $dropoff, $location, $comments, $vehicle_no, $color, $time_stamp);
						$counter = 0;      			//########### added this to make unique ids ###########
						while ($stmt->fetch()){
							if ($color == 'green'){
								echo "<tr class='success' id = $counter>";
							}
							else {
								echo "<tr class='danger' id = $counter>";
							}
							echo "<td class='editable-col' col-index='0'>$time_stamp</td>"; //########### added class and index ###########
							echo "<td class='editable-col' col-index='1'>$name</td>";
							echo "<td class='editable-col' col-index='2'>$uoid</td>";
							echo "<td class='editable-col' col-index='3'>$email</td>";
							echo "<td class='editable-col' col-index='4'>$phone</td>";
							echo "<td class='editable-col' col-index='5'>$pickup_time</td>";
							echo "<td class='editable-col' col-index='6'>$party</td>";
							echo "<td class='editable-col' col-index='7'>$pickup</td>";
							echo "<td class='editable-col' col-index='8'>$dropoff</td>";
							echo "<td class='editable-col' col-index='9'>$location</td>";	
							echo "<td class='editable-col' col-index='10'>$comments</td>";
							echo "<td class='editable-col' col-index='11'>$vehicle_no</td>";
							echo "<td><button id = 'editButton' onclick = foo('$counter')>edit</button></td>";
							
							echo "</tr>";
							$counter++;  			//###########
						}

						$sql_con->close();
					?>

					</tbody>
				</table>
			</div>
		</div>
		<!-- //########### from here -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script>
         function foo(arg){
        	var row = document.getElementById(arg);
        	var ebutt = document.getElementById('editButton');
        	row.contentEditable = true;
        	ebutt.contentEditable = false;
        	//ebutt.value="Save";    //###########can't figure out name of button text property##########
        	row.focus();
        	
        	$(document).ready(function(){
				$('td.editable-col').on('focusout', function() {
					data = {};
					data['val'] = $(this).text();
					data['id'] = $(this).parent('tr').attr(arg);
					data['index'] = $(this).attr('col-index');
					$.ajax({   
						  type: "POST",  
						  url: "update.php",  
						  cache:false,  
						  data: data,
						  dataType: "json",       
					});
				});
        	});
        	row.onfocusout = function() {
        		this.contentEditable = false;
        	}
         }
        </script>
        <!-- //########### to here -->
	</body>
</html>



















