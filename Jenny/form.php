<?php

include('connectionData.php');

// Create connection
$sql_con = new mysqli($server, $username, $password, $dbname, $port);

// Check connection
if ($sql_con->connect_error) {
    die("Connection failed: ".$sql_con->connect_error);
} 

?>


<html>
    <head>
    	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sign Up Form</title>
        <link rel="stylesheet" href="static/css/normalize.css">
        <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="static/css/main.css">
    </head>
    
    <body>

<!--     	<div style = "text-align: center">
			<img src="http://pages.uoregon.edu/saferide/images/saferide_banner.png" alt="Logo" style="width:376px;height:85px;" align="middle">
		</div>
 -->
        <form action="form.php" method='POST'>
            <h1>Sign Up</h1>

            <fieldset>

                <legend><span class="number">1</span> Riders Basic Info</legend>
                <label for="name">Name: </label>
                <input type="text" id="name" name="user_name">

                <label for="uoid">UO ID Number: </label>
                <input type="text" maxlength="9" id="uoid" name="user_uoid">

                <label for="mail">Email: </label>
                <input type="email" id="mail" name="user_email">

                <label for="phone">Phone Number: </label>
                <input type="text" maxlength="10" id="phone" name="user_phone">


            </fieldset>

            <fieldset>
                <legend><span class="number">2</span> Scheduling Info</legend>

                <label for="time">Pick-up Time:</label>
                <select id="hour" name="user_time[]">
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
		<label id="time_label"> : </label>
		<select id="minute" name="user_time[]">
			<option value="05">00</option>	
			<option value="05">05</option>
			<option value="05">10</option>
			<option value="05">15</option>
			<option value="05">20</option>
			<option value="05">25</option>
			<option value="05">30</option>
			<option value="05">35</option>
			<option value="05">40</option>
			<option value="05">45</option>
			<option value="05">50</option>
			<option value="05">55</option>
		</select>


                <label for="party">Number of Riders:</label>
                <input type="radio" id="one" value="1" name="party_number"><label for="one" class="light">1</label>
                <br>
                <input type="radio" id="two" value="2" name="party_number"><label for="two" class="light">2</label>
                <br>
                <input type="radio" id="one" value="3" name="party_number"><label for="three" class="light">3</label>


            </fieldset>
            
            
            <fieldset>
                <legend><span class="number">3</span> Locations Info</legend>

                <label for="pickup">Pick-up Address: </label>
                <textarea type="text" id="pickup" name="user_pickup"></textarea>

                <label for="dropoff">Drop-Off Address: </label>
                <textarea type="text" id="dropoff" name="user_dropoff"></textarea>

                <label for="location">Common locations:</label>
                <select id="location" name="user_location">
                    <optgroup label="On-Campus">
                        <option value="Deschutes">Deschutes</option>
                        <option value="Knight Library">Knight Library</option>
                        <option value="Science Library">Science Library</option>
                        <option value="Law Library">Law Library</option>
                        <option value="Recreation Center">Recreation Center</option>
                        <option value="Dorm">Dorm</option>
                    </optgroup>
                    <optgroup label="Off-Campus">
                        <option value="vrc">Valley River Center</option>
                        <option value="gateway">Gateway Mall</option>
                    </optgroup>
                </select>
            </fieldset>

            <button type="submit">Submit</button>
        </form>
	
		<hr>
		<p>Return to <a href="http://pages.uoregon.edu/saferide/index.html">SafeRide</a>.</p>
		<hr>

<?php

	try {

		$full_name = $_POST['user_name'];
        $user_uoid = $_POST['user_uoid'];
        $user_email = $_POST['user_email'];
        $user_phone = $_POST['user_phone'];
        $user_time = implode(':', $_POST['user_time']);
        $party_number = $_POST['party_number'];
        $user_pickup = $_POST['user_pickup'];
        $user_dropoff = $_POST['user_dropoff'];
        $user_location = $_POST['user_location'];


		// prepared statements
		// $stmt = $sql_con->prepare("INSERT INTO schedules (full_name, user_uoid, user_email, user_phone, user_time, party_number, user_pickup, user_dropoff, user_location) VALUES (?,?,?,?,?,?,?,?,?)");
        $stmt = $sql_con->prepare("INSERT INTO schedules (full_name, uoid, email, phone, pickup_time, party, pickup, dropoff, location) VALUES (?,?,?,?,?,?,?,?,?)");



		// $stmt->bind_param("ss", $full_name, $user_uoid, $user_email, $user_phone, $user_time, $party_number, $user_pickup, $user_dropoff, $user_location);
        $stmt->bind_param("sssssssss", $full_name, $user_uoid, $user_email, $user_phone, $user_time, $party_number, $user_pickup, $user_dropoff,$user_location);

		$stmt->execute();

		echo "New records inserted successfully";
	}
	catch(PDOException $e)
    {
    	echo "Error: ".$e->getMessage();
    }

    if($_POST){
		header("location:thankyou.html");
		exit();
	}
?>



    </body>
</html>
